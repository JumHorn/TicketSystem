-- MySQL dump 10.13  Distrib 5.7.15, for Win64 (x86_64)
--
-- Host: localhost    Database: movieticketsystem
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cinema`
--

DROP TABLE IF EXISTS `cinema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cinema` (
  `cinemaID` int(11) NOT NULL,
  `cinemaName` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`cinemaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cinema`
--

LOCK TABLES `cinema` WRITE;
/*!40000 ALTER TABLE `cinema` DISABLE KEYS */;
INSERT INTO `cinema` VALUES (1,'思明电影院','11111111','思明区北路'),(2,'金逸影城（名汇店）','22222222','思明区名汇广场');
/*!40000 ALTER TABLE `cinema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `movieID` int(11) NOT NULL,
  `movieName` varchar(50) NOT NULL,
  `director` varchar(30) DEFAULT NULL,
  `actors` varchar(100) DEFAULT NULL,
  `length_min` int(11) DEFAULT NULL,
  `releaseDate` date DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`movieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (333,'蝙蝠侠：黑暗骑士',NULL,NULL,NULL,'2008-07-18',NULL),(2222,'肖申克的救赎',NULL,NULL,NULL,'1994-09-13',NULL),(123123132,'死亡骑士',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moviesession`
--

DROP TABLE IF EXISTS `moviesession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moviesession` (
  `sessionID` int(11) NOT NULL AUTO_INCREMENT,
  `onlineMovieID` int(11) NOT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `showDay` date DEFAULT NULL,
  `hallID` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`sessionID`),
  KEY `FK_om` (`onlineMovieID`),
  CONSTRAINT `FK_om` FOREIGN KEY (`onlineMovieID`) REFERENCES `onlinemovie` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moviesession`
--

LOCK TABLES `moviesession` WRITE;
/*!40000 ALTER TABLE `moviesession` DISABLE KEYS */;
INSERT INTO `moviesession` VALUES (1,1,'2016-10-12 13:30:00','2016-10-12 15:30:00','2016-10-12',NULL,50),(2,1,'2016-10-12 16:30:00','2016-10-12 18:30:00','2016-10-12',NULL,50),(3,1,'2016-10-13 13:30:00','2016-10-13 15:30:00','2016-10-13',NULL,50),(4,1,'2016-10-11 14:00:00','2016-10-11 16:00:00','2016-10-11',NULL,50),(5,2,'2017-10-12 13:30:00','2017-10-12 15:30:00','2017-10-12',NULL,40),(6,2,'2017-10-13 13:30:00','2017-10-13 15:30:00','2017-10-13',NULL,40),(7,2,'2017-10-12 16:00:00','2017-10-12 17:30:00','2017-10-12',NULL,40);
/*!40000 ALTER TABLE `moviesession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinemovie`
--

DROP TABLE IF EXISTS `onlinemovie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinemovie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `movieID` int(11) NOT NULL,
  `cinemaID` int(11) NOT NULL,
  `price` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_c` (`cinemaID`),
  KEY `fk_m` (`movieID`),
  CONSTRAINT `fk_c` FOREIGN KEY (`cinemaID`) REFERENCES `cinema` (`cinemaID`),
  CONSTRAINT `fk_m` FOREIGN KEY (`movieID`) REFERENCES `movie` (`movieID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinemovie`
--

LOCK TABLES `onlinemovie` WRITE;
/*!40000 ALTER TABLE `onlinemovie` DISABLE KEYS */;
INSERT INTO `onlinemovie` VALUES (1,2222,1,50),(2,333,1,40),(3,2222,2,60);
/*!40000 ALTER TABLE `onlinemovie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `time` date DEFAULT NULL,
  `pay` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `movieID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK1` (`userID`),
  CONSTRAINT `FK1` FOREIGN KEY (`userID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1000,'2016-10-06',39,1,16),(1001,'2016-10-09',12,1,17);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `phoneNum` varchar(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` char(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2016032 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'hj','Jum',NULL,NULL,NULL,'123'),(2016001,'we','we123','18702016001','2016-10-04','we@qq.com','123'),(2016002,'qt','qt123','18702016002','2016-10-04','qt@qq.com','123'),(2016003,'gg','gg123','18702016003','2016-10-04','gg@qq.com','123'),(2016004,'sd','sd123','18702016004','2016-10-04','sd@qq.com','123'),(2016005,'bd','bd123','18702016005','2016-10-04','bd@qq.com','123'),(2016006,'cc','cc123','18702016006','2016-10-04','cc@qq.com','123'),(2016007,'js','js123','18702016007','2016-10-04','js@qq.com','123'),(2016008,'ae','ae123','18702016008','2016-10-04','ae@qq.com','123'),(2016009,'ne','ne123','18702016009','2016-10-04','ne@qq.com','123'),(2016010,'axx','axx123','18702016010','2016-10-04','axx@qq.com','123'),(2016011,'sz','sz123','18702016011','2016-10-04','sz@qq.com','123'),(2016012,'qj','qj123','18702016012','2016-10-04','qj@qq.com','123'),(2016013,'bf','bf123','18702016013','2016-10-04','bf@qq.com','123'),(2016014,'ky','ky123','18702016014','2016-10-04','ky@qq.com','123'),(2016015,'og','og123','18702016015','2016-10-04','og@qq.com','123'),(2016016,'db','db123','18702016016','2016-10-04','db@qq.com','123'),(2016017,'mt','mt123','18702016017','2016-10-04','mt@qq.com','123'),(2016018,'cd','cd123','18702016018','2016-10-04','cd@qq.com','123'),(2016019,'vc','vc123','18702016019','2016-10-04','vc@qq.com','123'),(2016020,'mv','mv123','18702016020','2016-10-04','mv@qq.com','123'),(2016021,'uf','uf123','18702016021','2016-10-04','uf@qq.com','123'),(2016022,'od','od123','18702016022','2016-10-04','od@qq.com','123'),(2016027,'寮犱笁',NULL,NULL,NULL,NULL,NULL),(2016028,'灏忔槑','Jum','18750201580','2016-01-01','zheyangkaishi@gmail.com','123'),(2016029,'鏉庡洓','lisi','18750163254','2016-01-01','45612854@qq.com','123'),(2016030,'鐜嬩簲','wang','13756421548','2016-01-30','45872135@qq.com','123'),(2016031,'sb','2b','','2013-05-05','21@sina.com','1234');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-12  0:01:15
