package user;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.mysql.jdbc.Statement;

@SuppressWarnings("serial")
public class Login extends DefaultUI {
	private JButton name = new JButton();
	private JButton password = new JButton();
	private JLabel name1 = new JLabel();
	private JLabel password1 = new JLabel();
	private JTextField name2 = new JTextField();
	private JPasswordField password2 = new JPasswordField();

	public Login() {
		// login properties登陆框属性
		setLayout(new GridLayout(3, 2, 30, 30));
		setSize(300, 200);
		setResizable(false);
		setTitle("Login");
		setVisible(true);
//		setAlwaysOnTop(true);        //总是在最前
		setLocationRelativeTo(null);// 从屏幕中央启动
		user_login.setEnabled(false);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				user_login.setEnabled(true);
			}
		});

		// label name1
		name1.setText("账号");
		name1.setHorizontalAlignment(SwingConstants.CENTER);
		add(name1);
		//
		// 账号输入框
		// name2.setText("用户名/手机号");
		add(name2);
		// label password1
		password1.setText("密码");
		password1.setHorizontalAlignment(SwingConstants.CENTER);
		add(password1);
		// 密码输入框
		add(password2);
		// JButton name
		name.setText("登陆");
		name.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nameActionPerformed(e);
			}
		});
		add(name);
		//
		// JButton password
		password.setText("注册");
		password.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO 自动生成的方法存根
				passwordActionPerformed(e);
			}
		});
		add(password);
		//

	}

	// 登陆按钮事件
	private void nameActionPerformed(ActionEvent e) {
		String query;
		try {
			query = "select ID,password,nickname from user;";
			Connection con = new DatabaseConnection().DbConnect();
			Statement stmt = (Statement) con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				if (rs.getString(1).trim().equals(name2.getText().trim())
						&& rs.getString(2)
								.trim()
								.equals(String.valueOf(password2.getPassword()))) {
					JOptionPane.showMessageDialog(null, "登陆成功", "welcome",
							JOptionPane.PLAIN_MESSAGE);
					//设置登录button文字
					user_login.setText("welcome "+rs.getString(3));
					userID=rs.getInt(1);
//					user_login.setEnabled(false);
					break;
				}
			}
			if (!rs.next()) {
				JOptionPane.showMessageDialog(null, "登陆失败", "welcome",
						JOptionPane.PLAIN_MESSAGE);
			}
			con.close();
			this.dispose(); // 销毁当前窗口
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "mysql错误", "出错提示",
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	private void passwordActionPerformed(ActionEvent e) {
		this.dispose();
		user_login.setEnabled(true);
		new Register();
	}
}