package user;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

//import MovieQuery.TopFrame;
import order.Order_manage;

@SuppressWarnings("serial")
public class Management extends DefaultUI {

	private JPanel menu = new JPanel();
	public JPanel poster; // 便于static的usermanage调用
	private JLabel head = new JLabel();
	private CardLayout cardLayout = new CardLayout();

	public Management() {
		setLayout(new BorderLayout());
		setTitle("welcome");
		// setVisible(true); 放在最前面导致图片不显示
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menu.setLayout(new GridLayout(3, 1));
		add(menu, BorderLayout.WEST);

		head.setText("welcome to ticket mall");
		head.setHorizontalAlignment(SwingConstants.CENTER);
		head.setFont(new Font("微软雅黑", 1, 72));
		head.setForeground(Color.BLUE);
		add(head, BorderLayout.NORTH);

		// 用户登录button
		user_login.setText("用户登录");
		menu.add(user_login);

		// 我的订单
		myticket.setText("我的订单");

		menu.add(myticket);

		// 电影搜索
		movie.setText("影片搜索");
		menu.add(movie);

		// 设置窗体背景 方法1
		// poster = new JPanel() {
		// public void paintComponent(Graphics g) {
		// ImageIcon icon = new ImageIcon("img/大圣归来.jpg");
		// // 图片随窗体大小而变化
		// g.drawImage(icon.getImage(), 0, 0, this.getSize().width,
		// this.getSize().height, this);
		// }
		// };
		// 设置窗体背景 方法2
		poster = new JPanel();
		poster.setLayout(cardLayout);

		// 改变鼠标形状
		// String url = "img/cursor_r.png"; //储存鼠标图片的位置
		// Toolkit tk = Toolkit.getDefaultToolkit();
		// Image image = new ImageIcon(url).getImage();
		// Cursor cursor = tk.createCustomCursor(image, new Point(10, 10),
		// "norm");
		// poster.setCursor(cursor); //也可以是其他组件

		// 将图片绘制到面板中
		for (int i = 0; i < 3; i++) {
			final int j = i;
			JPanel img = new JPanel() {
				protected void paintComponent(Graphics g) {
					g.drawImage(
							Toolkit.getDefaultToolkit().getImage(
									"img/poster" + j + ".jpg"), 0, 0,
							this.getSize().width, this.getSize().height, this);
				}
			};
			// 将图片面板添加到使用了CardLayout的容器面板中
			poster.add("poster" + j, img);
		}
		setmouseaction();
		add(poster, BorderLayout.CENTER);

		myticket.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(user_login.getText().equals("用户登录"))
				{
					JOptionPane.showMessageDialog(null, "请先登录", "提示信息",
							JOptionPane.PLAIN_MESSAGE);
				}
				else {
					myticket.setEnabled(false);
					new Order_manage();
				}
			}
		});
		
		movie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(user_login.getText().equals("用户登录"))
				{
					JOptionPane.showMessageDialog(null, "请先登录", "提示信息",
							JOptionPane.PLAIN_MESSAGE);
				}
				else {
//					new TopFrame().setVisible(true);
				}
			}
		});

		user_login.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// user_login.setEnabled(false); //设置按钮不可用
				if (user_login.getText().equals("用户登录"))
					new Login();
				// new Login().addWindowListener(new WindowAdapter() {
				// public void windowClosing(WindowEvent e)
				// {
				// user_login.setEnabled(true);
				// }
				// }); //设置关闭按钮时窗口可用
				// user_loginActionListener(e);
			}
		});
		setVisible(true);
	}

	// private void user_loginActionListener(ActionEvent e)
	// {
	// user_login.setEnabled(false);
	// new Login();
	// }
	private void setmouseaction() {
		poster.addMouseListener(new MouseAdapter() {
			// public void mouseEntered(MouseEvent e) {
			// }

			public void mouseClicked(MouseEvent e) {
				// 点击执行翻页
				if (e.getX() > poster.getWidth() / 2)
					cardLayout.next(poster); // 切换下一个选项卡
				else
					cardLayout.previous(poster);// 切换至上一张选项卡
			}
		});

		poster.addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				// 改变鼠标形状
				if (e.getX() > poster.getWidth() / 2) {
					String url = "img/cursor_r.png"; // 储存鼠标图片的位置
					Toolkit tk = Toolkit.getDefaultToolkit();
					Image image = new ImageIcon(url).getImage();
					Cursor cursor = tk.createCustomCursor(image, new Point(10,
							10), "norm");
					poster.setCursor(cursor); // 也可以是其他组件
				} else {
					String url = "img/cursor_l.png"; // 储存鼠标图片的位置
					Toolkit tk = Toolkit.getDefaultToolkit();
					Image image = new ImageIcon(url).getImage();
					Cursor cursor = tk.createCustomCursor(image, new Point(10,
							10), "norm");
					poster.setCursor(cursor); // 也可以是其他组件
				}
			}
		});
	}
	// private void myticketActionPerformed(ActionEvent e)
	// {
	// new Order_manage();
	// }
}
