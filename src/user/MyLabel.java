package user;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class MyLabel extends JLabel{
	public MyLabel()
	{
		setHorizontalAlignment(SwingConstants.CENTER);
	}
}
