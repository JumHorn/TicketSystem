package user;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import com.mysql.jdbc.Statement;

@SuppressWarnings("serial")
public class Register extends DefaultUI {

	private JPanel primarypanel = new JPanel();
	private JPanel bottompane = new JPanel();

	private MyLabel label1 = new MyLabel();
	private MyTextField jTextField1 = new MyTextField();

	private MyLabel label2 = new MyLabel();
	private MyTextField jTextField2 = new MyTextField();

	private MyLabel label3 = new MyLabel();
	private JPasswordField jPasswordField3 = new JPasswordField();

	private MyLabel label4 = new MyLabel();
	private MyTextField jTextField4 = new MyTextField();

	private MyLabel label5 = new MyLabel();
	private JLabel jLabel5y = new JLabel("年");
	private JLabel jLabel5m = new JLabel("月");
	private JLabel jLabel5d = new JLabel("日");
	private JComboBox<Integer> jComboBox5y = new JComboBox<Integer>();
	private JComboBox<Integer> jComboBox5m = new JComboBox<Integer>();
	private JComboBox<Integer> jComboBox5d = new JComboBox<Integer>();
	private JPanel datepanel5 = new JPanel();

	private MyLabel label6 = new MyLabel();
	private MyTextField jTextField6 = new MyTextField();

	private JButton jButton7 = new JButton();

	public Register() {
		setdate();
		setSize(350, 350);
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle("用户注册");
		setLayout(new BorderLayout());
		setVisible(true);

		add(primarypanel, BorderLayout.CENTER);

		add(bottompane, BorderLayout.SOUTH);

		// 设置primarypanel属性
		primarypanel.setLayout(new GridLayout(6, 2, 10, 10));

		// 设置bottompanel属性
		bottompane.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

		// 第1组
		label1.setText("姓名");
		primarypanel.add(label1);
		primarypanel.add(jTextField1);

		// 第2组
		label2.setText("昵称");
		primarypanel.add(label2);
		primarypanel.add(jTextField2);

		// 第3组
		label3.setText("密码");
		primarypanel.add(label3);
		primarypanel.add(jPasswordField3);

		// 第4组
		label4.setText("电话");
		primarypanel.add(label4);
		primarypanel.add(jTextField4);

		// 第5组
		label5.setText("生日");
		datepanel5.setLayout(new GridLayout(2, 3));
		datepanel5.add(jLabel5y);
		datepanel5.add(jLabel5m);
		datepanel5.add(jLabel5d); // 日期设置是和月份相关的，以后再讨论
		datepanel5.add(jComboBox5y);
		datepanel5.add(jComboBox5m);
		datepanel5.add(jComboBox5d);
		primarypanel.add(label5);
		primarypanel.add(datepanel5);

		// 第6组
		label6.setText("邮箱");
		primarypanel.add(label6);
		primarypanel.add(jTextField6);

		// 第7组
		jButton7.setText("注册");
		jButton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				registerActionPerformed(e);
			}
		});
		bottompane.add(jButton7);
	}

	private void registerActionPerformed(ActionEvent e) {
		String query;
		try {
			query = String
					.format("insert into user(name,nickname,phoneNum,birthday,email,password)values('%s','%s','%s','%d-%d-%d','%s','%s')",
							jTextField1.getText(), jTextField2.getText(),
							jTextField4.getText(),
							jComboBox5y.getSelectedItem(),
							jComboBox5m.getSelectedItem(),
							jComboBox5d.getSelectedItem(),
							jTextField6.getText(),
							String.valueOf(jPasswordField3.getPassword()));
			Connection con = new DatabaseConnection().DbConnect();
			Statement stmt = (Statement) con.createStatement();
			int result = stmt.executeUpdate(query);
			con.close();
			if (result != -1) {
				JOptionPane.showMessageDialog(null, "注册成功", "welcome",
						JOptionPane.PLAIN_MESSAGE);
			}
			this.dispose(); // 销毁当前窗口
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "mysql错误", "出错提示",
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	private void setdate() {
		int i = 0;
		for (i = 2016; i > 1900; i--) {
			jComboBox5y.addItem(i);
		}
		for (i = 1; i < 13; i++) {
			jComboBox5m.addItem(i);
		}

		// jComboBox5m.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// checkdate(e);
		// }});
		// jComboBox5y.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// checkdate(e);
		// }
		// });
		jComboBox5d.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				checkdate(e);
			}
		});
	}

	private void checkdate(MouseEvent e) {
		jComboBox5d.removeAllItems();
		switch ((int) jComboBox5m.getSelectedItem()) {
		case 4:
			;
		case 6:
			;
		case 9:
			;
		case 11:
			for (int i = 1; i < 31; i++) {
				jComboBox5d.addItem(i);
			}
			break;
		case 2:
			if ((int) jComboBox5y.getSelectedItem() % 400 == 0
					|| ((int) jComboBox5y.getSelectedItem() % 4 == 0)
					&& (int) jComboBox5y.getSelectedItem() % 100 != 0) {
				for (int i = 1; i < 30; i++) {
					jComboBox5d.addItem(i);
				}
			} else {
				for (int i = 1; i < 29; i++) {
					jComboBox5d.addItem(i);
				}
			}
			break;
		default:
			for (int i = 1; i < 32; i++) {
				jComboBox5d.addItem(i);
			}
			break;
		}
	}
}
