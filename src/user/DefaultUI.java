package user;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class DefaultUI extends JFrame{
	
	protected static MyButton user_login = new MyButton();
	protected static MyButton myticket = new MyButton();
	protected static MyButton movie = new MyButton();
	
	protected static int userID;    //设置全局变量
	
	public DefaultUI()
	{
		//设置title图片
		String src = "img/logo.png";             //图片路径
		ImageIcon image=new ImageIcon(src);      //创建图片对象
		setIconImage(image.getImage());          //设置图标	
		
//		//设置背景图片
//		image.setDescription("img/backgroundimage.jpg");
		
		setTitle("欢迎使用本系统");
		
		setExtendedState(JFrame.MAXIMIZED_BOTH); //设置窗口最大化
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);  //设置只关闭当前窗口
		
//		setLocationRelativeTo(null);//从屏幕中央启动
		
		
	}
}
