package seat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import user.DefaultUI;

@SuppressWarnings("serial")
public class Seat extends DefaultUI{
	private JLabel Jlable1= new JLabel("屏幕");
	private JPanel seatpanel = new JPanel();
	private JTable seat_state = new JTable();
	public Seat()
	{
		setLayout(new BorderLayout());
		setSize(400, 400);
		setResizable(false);
		setLocationRelativeTo(null);// 从屏幕中央启动
		//设置label
		Jlable1.setHorizontalAlignment(SwingConstants.CENTER);
		Jlable1.setFont(new Font("微软雅黑",Font.PLAIN,14));
		Jlable1.setForeground(Color.BLUE);
		add(Jlable1,BorderLayout.NORTH);
		
		setseatpanel();
		add(seatpanel);
		
		setVisible(true);   //设置可见
	}
	private void setseatpanel()
	{
		seatpanel.add(seat_state);
		int[][] seat = new int[10][25];   //用于存储座位信息，和是否选中的状态
		Object[][] seatstate = new Object[10][25];  //设置作为表格
		seat_state.setModel(new DefaultTableModel(seatstate, seatstate));
		seat_state.setRowSelectionAllowed(true);
		seat_state.setColumnSelectionAllowed(true);   
//		seat_state.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	}
}
