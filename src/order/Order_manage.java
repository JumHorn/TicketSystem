package order;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.management.modelmbean.ModelMBean;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.Statement;

import user.DatabaseConnection;
import user.DefaultUI;

@SuppressWarnings("serial")
public class Order_manage extends DefaultUI {
	private JScrollPane primarypanel = new JScrollPane();
	private JTable ordertable;

	public Order_manage() {
		setTitle("我的订单");
		setResizable(false);
		setLayout(new BorderLayout());
		setSize(400, 400);
		setLocationRelativeTo(null);// 从屏幕中央启动
		setVisible(true);
		// 主界面属性
		add(primarypanel, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myticket.setEnabled(true);
			}
		});
		
		setordertable(); // 设置表格属性

		primarypanel.setViewportView(ordertable); // 显示表
	}

	private void setordertable() {
		Vector<Vector<Object>> content = new Vector<Vector<Object>>();
		String query;
		try {
			query = String.format("select * from `order`;");
			Connection con = new DatabaseConnection().DbConnect();
			Statement stmt = (Statement) con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Vector<Object> row = new Vector<Object>();
				row.add(rs.getString(1));
				row.add(rs.getDate(2));
				row.add(rs.getInt(3));
				row.add(rs.getInt(5));
				content.add(row);
			}
			con.close();
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "无法连接到服务器", "出错提示",
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		// ordertable = new JTable(content, title); //下面两行等价
		ordertable = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		Vector<Object> title = new Vector<Object>();
		title.add("影名");
		title.add("时间");
		title.add("金钱");
		title.add("影院");
		model.setDataVector(content, title);
		ordertable.setModel(model);
	}
}
